import string
from collections import defaultdict

def count_word_frequency(file_path):
    word_freq = defaultdict(int)

    with open(file_path, 'r') as file:
        text = file.read()
        text = text.lower()
        translator = str.maketrans('', '', string.punctuation)
        text = text.translate(translator)
        words = text.split()

        for word in words:
            word_freq[word] += 1

    return word_freq

def display_word_frequency(word_freq):
    sorted_freq = sorted(word_freq.items(), key=lambda x: x[1], reverse=True)
    for word, freq in sorted_freq:
        print(f'{word}: {freq}')

# Prompt the user for the file path
file_path = input('Enter the path to the text file: ')

# Count the word frequency
word_frequency = count_word_frequency(file_path)

# Display the results
display_word_frequency(word_frequency)
