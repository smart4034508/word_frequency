# Word Frequency Counter

This is a simple Python script that counts the frequency of words in a given text document.

## Getting Started

1. Clone this repository to your local machine.

2. Make sure you have Python 3.x installed.

3. Install the required dependencies by running the following command:

4. Open the `word_frequency_counter.py` file and update the `text_file_path` variable with the path to your desired text document.

## Usage

To run the script and get the word frequency count, use the following command in your terminal:

python word_frequency_counter.py


The script will read the specified text document, count the frequency of each word, and display the result in descending order.

## Example

Consider the following text file (`sample_text.txt`):

This is a sample text file.
It contains several words for testing.
The word frequency counter will count the occurrences of each word.


Running the script with this text file will produce the following output:
Word Frequency Count:
word: 3
the: 2
count: 2
file: 1
frequency: 1
occurrences: 1
testing: 1
several: 1
contains: 1
for: 1
this: 1
is: 1
sample: 1

